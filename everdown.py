#!-*- coding:utf-8 -*-

import json
import io
from evernote.api.client import EvernoteClient
from bs4 import BeautifulSoup, NavigableString, Comment, Declaration


def load_capters(filename):
    """
        >>> load_capters("test/assets/chapters.json")
        [u'moge', u'fuga', u'hoge']
    """
    raw_str = io.open(filename).read()
    return json.loads(raw_str)

def enml_to_plaintext(enml_string):
    """
        >>> print enml_to_plaintext("<en-note>hoge</en-note>")
        hoge
        >>> print enml_to_plaintext("<en-note><div>hoge<br/>fuga</div><div>moge</div></en-note>")
        hoge
        fuga
        moge
        >>> enml_to_plaintext("<en-note><div>fuga</div><div><br clear='none'/></div><div>kuma</div></en-note>")
        u'fuga\\n\\nkuma'
    """
    soup = BeautifulSoup(enml_string)
    note = soup.find("en-note")
    #print note.prettify()
    #for string in note.strings:
    #    print string
    return u''.join(keep_brank_line(note)).strip("\n").translate({
        0xA0: u' ',
        })

blockTags = frozenset(['p', 'div', 'table', 'dl', 'ul',
  'ol', 'form', 'address', 'blockquote', 'h1', 'h2', 'h3',
  'h4', 'h5', 'h6', 'fieldset', 'hr', 'pre'
  'article', 'aside', 'dialog', 'figure', 'footer',
  'header', 'legend', 'nav', 'section'])
newlineTags = frozenset(['br'])

def keep_brank_line(soup):
    if isinstance(soup, NavigableString):
    # 空白だけでも返すように，``and soup.strip()'' を消しました．
    # 英数字前後の空白を誤って取り除くことを防ぎます．
        if type(soup) not in (Comment, Declaration):
            yield soup
    elif soup.name not in ('script', 'style'):
    # ブロック要素の前後に改行を入れるようにしました．
        isBlock = soup.name in blockTags
        isNewLine = soup.name in newlineTags
        for c in soup.contents:
            for g in keep_brank_line(c):
                yield g
        if isBlock or isNewLine:
            yield u'\n'

def export_chapter(chap_num, content):
    filename = "chapter_%02d.md" % chap_num
    f = io.open(filename, mode="w", encoding="utf-8")
    f.write(content)
    f.close()
    return filename

def main_drone(is_sandbox):
    from os import environ, system
    chap_ids = load_capters("chapters.json")
    ACCESS_TOKEN = environ.get("EVERNOTE_ACCESS_TOKEN")
    client = EvernoteClient(token=ACCESS_TOKEN, sandbox=is_sandbox)
    note_store = client.get_note_store()
    files = []
    for i, giud in enumerate(chap_ids):
        note = note_store.getNote(giud, True, True, True, True)
        files.append(export_chapter(i, enml_to_plaintext(note.content)))
    system("pandoc -o ./artifacts/book.epub %s" % ' '.join(files))

if __name__ == "__main__":
    from os import environ
    if environ.get("ENV") == "test":
        import doctest
        doctest.testmod()
    else:
        main_drone(environ.get("ENV") == "develop")

